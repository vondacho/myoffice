create sequence hibernate_sequence start with 1 increment by 1
create table Invoice (id bigint not null, createdBy varchar(255) not null, createdDate binary(255) not null, lastModifiedBy varchar(255), lastModifiedDate binary(255), amount double not null, closureDate binary(255), discount double not null, discountRate double not null, folderId varchar(255) not null, tax double not null, taxRate double not null, primary key (id))
create table Invoice_InvoiceDebtor (Invoice_id bigint not null, debtors_id bigint not null, primary key (Invoice_id, debtors_id))
create table Invoice_InvoicedProvision (Invoice_id bigint not null, provisions_id bigint not null, primary key (Invoice_id, provisions_id))
create table Invoice_InvoiceItem (Invoice_id bigint not null, items_id bigint not null, primary key (Invoice_id, items_id))
create table Invoice_InvoicePayment (Invoice_id bigint not null, payments_id bigint not null, primary key (Invoice_id, payments_id))
create table Invoice_InvoiceRecall (Invoice_id bigint not null, recalls_id bigint not null, primary key (Invoice_id, recalls_id))
create table InvoiceDebtor (id bigint not null, amount double not null, customerId varchar(255) not null, percentage double not null, remainingAmount double not null, invoice_id bigint, primary key (id))
create table InvoicedProvision (id bigint not null, consumedAmount double not null, invoice_id bigint, provision_id bigint, primary key (id))
create table InvoiceItem (id bigint not null, articleId varchar(255), price double not null, quantity double not null, tariff double not null, title varchar(255) not null, invoice_id bigint, primary key (id))
create table InvoicePayment (id bigint not null, amount double not null, debtor_id bigint, invoice_id bigint, reception_id bigint, primary key (id))
create table InvoiceRecall (id bigint not null, createdBy varchar(255) not null, createdDate binary(255) not null, lastModifiedBy varchar(255), lastModifiedDate binary(255), invoice_id bigint, primary key (id))
create table Provision (id bigint not null, createdBy varchar(255) not null, createdDate binary(255) not null, lastModifiedBy varchar(255), lastModifiedDate binary(255), amount double not null, folderId varchar(255) not null, remainingAmount double not null, reception_id bigint, primary key (id))
create table Reception (id bigint not null, createdBy varchar(255) not null, createdDate binary(255) not null, lastModifiedBy varchar(255), lastModifiedDate binary(255), channel integer not null, customerId varchar(255), sum double not null, ticket_id bigint, primary key (id))
create table Ticket (id bigint not null, createdBy varchar(255) not null, createdDate binary(255) not null, lastModifiedBy varchar(255), lastModifiedDate binary(255), folderId varchar(255) not null, number varchar(255) not null, primary key (id))
alter table Invoice_InvoiceDebtor add constraint UK_dbn5m74wmxjvvdkvqbv0dorhd unique (debtors_id)
alter table Invoice_InvoicedProvision add constraint UK_guvagdc5ullb7i0ucf7a293qo unique (provisions_id)
alter table Invoice_InvoiceItem add constraint UK_u0jcl7l1k3cj9q6t2yfvp0gr unique (items_id)
alter table Invoice_InvoicePayment add constraint UK_dmp9n3po0dqk9ct8pxde3mol2 unique (payments_id)
alter table Invoice_InvoiceRecall add constraint UK_50sy20o64m51453owxpqk8d5s unique (recalls_id)
alter table Ticket add constraint UK_3ghtbngiawc3mdifjbtjq8e2m unique (number)
alter table Invoice_InvoiceDebtor add constraint FK3an3rooylaqhhem0kam1xxl5t foreign key (debtors_id) references InvoiceDebtor
alter table Invoice_InvoiceDebtor add constraint FKm0sui59ka3rktsic031q1wbk6 foreign key (Invoice_id) references Invoice
alter table Invoice_InvoicedProvision add constraint FKb3cildk9ly196pi61tul50fqx foreign key (provisions_id) references InvoicedProvision
alter table Invoice_InvoicedProvision add constraint FK1xe0ocry7r9i32nniktt9hp5m foreign key (Invoice_id) references Invoice
alter table Invoice_InvoiceItem add constraint FKokg6b7yeuwvtp9xq3dk4ujtyk foreign key (items_id) references InvoiceItem
alter table Invoice_InvoiceItem add constraint FKpat06isffk5qao9yi4em3t4g2 foreign key (Invoice_id) references Invoice
alter table Invoice_InvoicePayment add constraint FK9fpp4s46kkommvrjyuc8wn7fi foreign key (payments_id) references InvoicePayment
alter table Invoice_InvoicePayment add constraint FKflj6qr0salfd5ygsomm15dt50 foreign key (Invoice_id) references Invoice
alter table Invoice_InvoiceRecall add constraint FKt4pct5dcqlbrxvemjykeo053p foreign key (recalls_id) references InvoiceRecall
alter table Invoice_InvoiceRecall add constraint FKlsu3e1kexkd4cyie36okb6v6t foreign key (Invoice_id) references Invoice
alter table InvoiceDebtor add constraint FKifwh40yhnp84awfcl9200mx56 foreign key (invoice_id) references Invoice
alter table InvoicedProvision add constraint FKhvohj2of98wru5gcn13oaonin foreign key (invoice_id) references Invoice
alter table InvoicedProvision add constraint FK3bek4rqxuhv4uluugjbt2jw2x foreign key (provision_id) references Provision
alter table InvoiceItem add constraint FKm1qstfhfwpv8oip90w8fdxfl3 foreign key (invoice_id) references Invoice
alter table InvoicePayment add constraint FK2molag42j7n4rcmhfiotfqo8h foreign key (debtor_id) references InvoiceDebtor
alter table InvoicePayment add constraint FK8jw19hg9d0k3dlx5p1guynn7n foreign key (invoice_id) references Invoice
alter table InvoicePayment add constraint FKeoomki7gxhm42spuy3l7kijbv foreign key (reception_id) references Reception
alter table InvoiceRecall add constraint FKju6sc34475dbtjy7v6jw5q8nt foreign key (invoice_id) references Invoice
alter table Provision add constraint FKqerqh63lnci3ncviuxjiudoby foreign key (reception_id) references Reception
alter table Reception add constraint FKl1vrlsxna7n3y9w5th4q0t3ew foreign key (ticket_id) references Ticket
