package edu.software.myoffice.invoicing.rest;

import edu.software.myoffice.invoicing.core.domainservices.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class InvoicingRestAdapter {
    @Autowired
    private InvoiceRepository invoiceRepository;

    @RequestMapping(value = "/invoices/{id}/close", method = RequestMethod.POST)
    public void close(@PathVariable Long id) {
    }
}
