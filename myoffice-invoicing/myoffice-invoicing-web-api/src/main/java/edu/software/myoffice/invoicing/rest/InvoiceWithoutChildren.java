package edu.software.myoffice.invoicing.rest;

import edu.software.myoffice.invoicing.core.domain.*;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

/**
 * Created by olivier on 21.12.16.
 */
@Projection(name = "invoiceWithoutChildren", types = { Invoice.class })
public interface InvoiceWithoutChildren {

    public Set<InvoiceItem> getItems();

    public Set<InvoicedProvision> getProvisions();

    public Set<InvoiceDebtor> getDebtors();

    public Set<InvoicePayment> getPayments();
}
