package edu.software.myoffice.invoicing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {
        "edu.software.myoffice.invoicing.core",
        "edu.software.myoffice.invoicing.persistence",
        "edu.software.myoffice.invoicing.rest"})
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

