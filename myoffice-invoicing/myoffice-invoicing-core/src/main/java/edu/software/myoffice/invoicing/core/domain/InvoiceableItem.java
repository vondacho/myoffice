package edu.software.myoffice.invoicing.core.domain;

/**
 * Created by olivier on 07.08.14.
 */
public interface InvoiceableItem {
    String getArticleId();

    String getTitle();

    Double getQuantity();

    Double getTariff();

    Double getPrice();
}
