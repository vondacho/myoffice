package edu.software.myoffice.invoicing.core.domain;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Entity(name = "Invoice")
@Data
public class Invoice extends AbstractAuditable<Long> {

    @NotNull
    private String folderId;

    private LocalDate closureDate;

    @NotNull
    private Double taxRate = 0.0;

    @NotNull
    private Double tax = 0.0;

    @NotNull
    private Double discountRate = 0.0;

    @NotNull
    private Double discount = 0.0;

    @NotNull
    private Double amount = 0.0;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<InvoiceItem> items;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    //@Size(min = 1)
    private Set<InvoiceDebtor> debtors;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<InvoicePayment> payments;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<InvoiceRecall> recalls;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<InvoicedProvision> provisions;
}
