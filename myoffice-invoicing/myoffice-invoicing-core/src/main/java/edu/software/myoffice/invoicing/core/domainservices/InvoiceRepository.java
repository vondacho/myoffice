package edu.software.myoffice.invoicing.core.domainservices;

import edu.software.myoffice.invoicing.core.domain.Invoice;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceRepository extends CrudRepository<Invoice, Long> {
}
