package edu.software.myoffice.invoicing.core.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Reception")
public class Reception extends AbstractAuditable<Long> {

    @NotNull
    private ReceptionChannel channel;
    private String customerId;
    @NotNull
    private Double sum;
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private Ticket ticket;

    public ReceptionChannel getChannel() {
        return channel;
    }

    public void setChannel(ReceptionChannel channel) {
        this.channel = channel;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
