package edu.software.myoffice.invoicing.core.domain;

import javax.validation.constraints.NotNull;

public class Recall extends AbstractAuditable<Long> {

    @NotNull
    private double payedSum;
    @NotNull
    private double remainingSum;
    @NotNull
    private int degree;

    public double getPayedSum() {
	return payedSum;
    }

    public void setPayedSum(double payedSum) {
	this.payedSum = payedSum;
    }

    public double getRemainingSum() {
	return remainingSum;
    }

    public void setRemainingSum(double remainingSum) {
	this.remainingSum = remainingSum;
    }

    public int getDegree() {
	return degree;
    }

    public void setDegree(int degree) {
	this.degree = degree;
    }
}
