package edu.software.myoffice.invoicing.persistence;

import org.hibernate.jpa.AvailableSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(basePackages = "edu.software.myoffice.invoicing.core.domainservices")
@EnableTransactionManagement
@EnableJpaAuditing
public class PersistenceConfig {

    @Bean
    public DataSource dataSource() {
        DataSource dataSource = inMemoryDataSource();
        return dataSource;
    }

    public DataSource inMemoryDataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        return builder
                .setType(EmbeddedDatabaseType.H2)
                //.addScript("invoicing_schema.sql")
                //.addScripts("invoicing_data.sql")
                .build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setPackagesToScan("edu.software.myoffice.invoicing.core.domain");
        emf.setDataSource(dataSource());

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(true);
        emf.setJpaVendorAdapter(vendorAdapter);
        emf.setJpaProperties(additionalProperties());
        emf.afterPropertiesSet();
        return emf;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(emf);
        return txManager;
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty(org.hibernate.cfg.AvailableSettings.HBM2DDL_AUTO, "create-drop");
        properties.setProperty(AvailableSettings.SCHEMA_GEN_DATABASE_ACTION, "none");
        properties.setProperty(AvailableSettings.SCHEMA_GEN_CREATE_SCHEMAS, "true");
        properties.setProperty(AvailableSettings.SCHEMA_GEN_SCRIPTS_ACTION, "create");
        properties.setProperty(AvailableSettings.SCHEMA_GEN_CREATE_SOURCE, "metadata");
        properties.setProperty(AvailableSettings.SCHEMA_GEN_SCRIPTS_CREATE_TARGET, "schema-create.sql");
        //properties.setProperty(org.hibernate.cfg.AvailableSettings.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
        return properties;
    }
}
