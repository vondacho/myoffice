package edu.software.myoffice.invoicing.core.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "InvoiceDebtor")
public class InvoiceDebtor extends AbstractPersistable<Long> {

    @NotNull
    private Double percentage;
    @NotNull
    private Double amount;
    @NotNull
    private Double remainingAmount;
    @NotNull
    private String customerId;
    @ManyToOne
    private Invoice invoice;

    public InvoiceDebtor(Invoice invoice, String customerId, double amount, double percentage) {
        this.invoice = invoice;
        this.customerId = customerId;
        this.amount = amount;
        this.percentage = percentage;
    }

    public Invoice getInvoice() {
	return invoice;
    }

    public void setInvoice(Invoice invoice) {
	this.invoice = invoice;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(Double remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    
}
