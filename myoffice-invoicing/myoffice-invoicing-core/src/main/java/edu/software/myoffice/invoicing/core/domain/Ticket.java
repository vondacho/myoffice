package edu.software.myoffice.invoicing.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Ticket")
public class Ticket extends AbstractAuditable<Long> {

    @NotNull
    @Column(unique=true)
    @Size(min=1)
    private String number;
    @NotNull
    private String folderId;

    public void setNumber(String number) { this.number = number;}

    public String getNumber() { return number; }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }
}
