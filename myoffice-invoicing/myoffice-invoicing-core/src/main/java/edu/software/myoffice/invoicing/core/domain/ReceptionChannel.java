package edu.software.myoffice.invoicing.core.domain;

public enum ReceptionChannel {
    TICKET, CASH
}
