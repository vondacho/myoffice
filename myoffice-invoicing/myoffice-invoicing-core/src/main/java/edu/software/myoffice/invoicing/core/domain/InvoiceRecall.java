package edu.software.myoffice.invoicing.core.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "InvoiceRecall")
public class InvoiceRecall extends Recall {

    @ManyToOne
    private Invoice invoice;

    public Invoice getInvoice() {
	return invoice;
    }

    public void setInvoice(Invoice invoice) {
	this.invoice = invoice;
    }

}
