package edu.software.myoffice.invoicing.core.domainservices;

import edu.software.myoffice.invoicing.core.domain.Invoice;
import edu.software.myoffice.invoicing.core.domain.InvoiceItem;

import java.util.OptionalDouble;

/**
 * Created by olivier on 21.12.16.
 */
public class Calculator {

    public void amount(Invoice invoice) {
        Double total = 0.0;
        for (InvoiceItem item : invoice.getItems()) {
            total += item.getPrice() * item.getQuantity();
        }
        discount(invoice, total);
        tax(invoice, total);
        invoice.setAmount(total);
    }

    private void tax(Invoice invoice, Double total) {
        OptionalDouble.of(invoice.getTaxRate()).ifPresent(tax -> {
            invoice.setTax(invoice.getTaxRate() * total); });
    }

    private void discount(Invoice invoice, Double total) {
    }
}
