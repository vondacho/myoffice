package edu.software.myoffice.invoicing.core.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "InvoiceItem")
public class InvoiceItem extends AbstractPersistable<Long> implements InvoiceableItem {

    @NotNull
    private Double quantity;
    @NotNull
    private Double tariff;
    @NotNull
    private Double price;
    @NotNull
    @Size(min = 1)
    private String title;
    private String articleId;

    @ManyToOne
    private Invoice invoice;

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public void setTariff(Double tariff) {
        this.tariff = tariff;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public Double getTariff() {
        return tariff;
    }

    public Double getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getArticleId() {
        return articleId;
    }

    public InvoiceItem(InvoiceableItem invoiceable) {
        this.setTitle(invoiceable.getTitle());
        this.setQuantity(invoiceable.getQuantity());
        this.setTariff(invoiceable.getTariff());
        this.setPrice(invoiceable.getPrice());
        this.setArticleId(invoiceable.getArticleId());
    }

    public InvoiceItem() {
    }
}
