package edu.software.myoffice.invoicing.core.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "InvoicePayment")
public class InvoicePayment extends AbstractPersistable<Long> {
    
    @ManyToOne
    private Reception reception;
    @ManyToOne
    private InvoiceDebtor debtor;
    @ManyToOne
    private Invoice invoice;
    @NotNull
    private Double amount;

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Reception getReception() {
        return reception;
    }

    public void setReception(Reception reception) {
        this.reception = reception;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public InvoiceDebtor getDebtor() {
        return debtor;
    }

    public void setDebtor(InvoiceDebtor debtor) {
        this.debtor = debtor;
    }
}
