package edu.software.myoffice.invoicing.core.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "InvoicedProvision")
public class InvoicedProvision extends AbstractPersistable<Long> {

    @ManyToOne
    private Invoice invoice;
    @ManyToOne
    private Provision provision;
    @NotNull
    private Double consumedAmount;


    public InvoicedProvision(Invoice invoice, Provision provision, Double consume) {
        this.invoice = invoice;
        this.provision = provision;
        this.consumedAmount = consume;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public Provision getProvision() {
        return provision;
    }

    public Double getConsumedAmount() {
        return consumedAmount;
    }

}
