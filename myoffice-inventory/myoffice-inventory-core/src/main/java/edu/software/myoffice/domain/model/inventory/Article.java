package edu.software.myoffice.domain.model.inventory;

import edu.software.myoffice.domain.model.auditing.AbstractAuditable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by olivier on 12.08.14.
 */
@Entity
@Table(name = "Article")
public class Article  extends AbstractAuditable<Long> {
    @NotNull
    private String reference;
    @NotNull
    private String title;
    private String mediaUrl;
    @NotNull
    private Integer count = 0;
    @NotNull
    private Status status;

    @ManyToOne
    private Category category;

    enum Status {
        IN_SALE, SOLD_OUT;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
