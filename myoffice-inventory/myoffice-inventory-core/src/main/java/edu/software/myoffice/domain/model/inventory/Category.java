package edu.software.myoffice.domain.model.inventory;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by olivier on 12.08.14.
 */
@Entity
@Table(name = "Category")
public class Category extends AbstractPersistable<Long> {
    @NotNull
    @Size(min=1)
    private String title;

    @ManyToOne(optional = true)
    private Category parent;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }
}
