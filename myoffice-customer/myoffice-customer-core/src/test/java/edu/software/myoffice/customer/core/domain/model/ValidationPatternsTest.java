package edu.software.myoffice.customer.core.domain.model;

import edu.software.myoffice.customer.core.domain.validation.ValidationPatterns;
import org.junit.Test;

import static edu.software.myoffice.customer.core.domain.util.RegexMatcher.matchesRegex;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;

public class ValidationPatternsTest {
    @Test
    public void emailTest() {
        assertThat("b@c.d", matchesRegex(ValidationPatterns.EMAIL));
        assertThat("b@c.d.e.f", matchesRegex(ValidationPatterns.EMAIL));
        assertThat("a.b@c.d", matchesRegex(ValidationPatterns.EMAIL));
        assertThat("a.b@c.d.e.f", matchesRegex(ValidationPatterns.EMAIL));
        assertThat("a", not(matchesRegex(ValidationPatterns.EMAIL)));
        assertThat("a.b@c", not(matchesRegex(ValidationPatterns.EMAIL)));
        assertThat("a@c", not(matchesRegex(ValidationPatterns.EMAIL)));
    }

    @Test
    public void phoneTest() {
        assertThat("032 724 52 94", matchesRegex(ValidationPatterns.PHONE));
        assertThat("xxx", not(matchesRegex(ValidationPatterns.PHONE)));
        assertThat("032 724 52 94xxx", not(matchesRegex(ValidationPatterns.PHONE)));
        assertThat("032 724 08 05", matchesRegex(ValidationPatterns.PHONE_CH));
        assertThat("099 999 99 99", matchesRegex(ValidationPatterns.PHONE_CH));
        assertThat("+99 99 999 99 99", matchesRegex(ValidationPatterns.PHONE_INTERNATIONAL));
        assertThat("099 999 99 99", not(matchesRegex(ValidationPatterns.PHONE_INTERNATIONAL)));
        assertThat("99 999 99 99", not(matchesRegex(ValidationPatterns.PHONE_INTERNATIONAL)));
        assertThat("099 999 99 9", not(matchesRegex(ValidationPatterns.PHONE_INTERNATIONAL)));
    }
}
