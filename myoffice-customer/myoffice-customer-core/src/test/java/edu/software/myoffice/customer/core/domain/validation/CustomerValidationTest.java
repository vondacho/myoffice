package edu.software.myoffice.customer.core.domain.validation;

import edu.software.myoffice.customer.core.domain.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static edu.software.myoffice.customer.core.domain.validation.Validators.DEFAULT_VALIDATOR;
import static edu.software.myoffice.customer.core.domain.validation.Validators.throwViolations;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class CustomerValidationTest {

    @Mock
    ConstraintViolation<Customer> constraintViolation;

    private interface AnyValidationGroup {

    }

    @Test(expected = ConstraintViolationException.class)
    public void throwConstraintViolationException() {
        //given
        Set<ConstraintViolation<Customer>> constraintViolations = Collections.singleton(constraintViolation);
        //when
        throwViolations(constraintViolations);
    }

    @Test
    public void doNotThrowConstraintViolationException() {
        //given
        Set<ConstraintViolation<Customer>> constraintViolations = Collections.emptySet();
        //when
        try {
            throwViolations(constraintViolations);
        }
        //then
        catch (RuntimeException e) {
            fail();
        }
    }

    @Test
    public void validCustomer_nominal() {
        assertThat(DEFAULT_VALIDATOR.validate(Customer.builder()
                .lastName("last name")
                .street("street")
                .city("city")
                .zip("zip")
                .country("country")
                .phone1(Optional.of(PhoneNumber.builder().number("+41792773259").kind(PhoneNumber.Kind.MOBILE).build()))
                .profile(Profile.builder().build())
                .social(Social.builder().build())
                .build()), empty());
    }

    @Test(expected = ConstraintViolationException.class)
    public void invalidCustomer_empty() {
        Validators.validate(DEFAULT_VALIDATOR, Customer.builder().build());
    }

    @Test(expected = ConstraintViolationException.class)
    public void invalidPhoneNumber_empty() {
        Validators.validate(PhoneNumber.builder().build());
    }

    @Test(expected = ConstraintViolationException.class)
    public void invalidEmailAddress_empty() {
        Validators.validate(EmailAddress.builder().build());
    }

    @Test(expected = ConstraintViolationException.class)
    public void invalidFolder_empty() {
        throwViolations(DEFAULT_VALIDATOR.validate(Folder.builder().build()));
    }

    @Test
    public void invalidCustomer_missingMandatoryAttributes() {
        //given
        Customer customer = Customer.builder()
                .profile(Profile.builder().build())
                .social(Social.builder().build())
                .build();
        //when
        Set<ConstraintViolation<Customer>> violationsSet = DEFAULT_VALIDATOR.validate(customer);
        //then
        assertThat(violationsSet, hasSize(5));
    }

    @Test
    public void invalidCustomer_missingPhoneNumber() {
        //given
        Customer customer = Customer.builder()
                .lastName("last name")
                .street("street")
                .city("city")
                .zip("zip")
                .country("country")
                .phone1(Optional.of(PhoneNumber.builder().kind(PhoneNumber.Kind.MOBILE).build()))
                .profile(Profile.builder().build())
                .social(Social.builder().build())
                .build();
        //when
        Set<ConstraintViolation<Customer>> violationsSet = DEFAULT_VALIDATOR.validate(customer);
        //then
        assertThat(violationsSet, hasSize(1));
    }
}
