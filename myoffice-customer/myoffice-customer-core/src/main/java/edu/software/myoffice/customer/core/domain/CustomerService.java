package edu.software.myoffice.customer.core.domain;

import edu.software.myoffice.customer.core.domain.model.Customer;

import java.util.UUID;

public interface CustomerService {

    Customer save(Customer customer);

    Customer save(Customer customer, UUID folderId);

    Customer operationA(UUID customerId);

    Iterable<Customer> operationB();

    void folderize();

    void sanitize();
}
