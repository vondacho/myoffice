package edu.software.myoffice.customer.core.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Optional;

public class OptionalValidator implements ConstraintValidator<OptionalValid, Optional<?>> {

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    private OptionalValid constraintAnnotation;

    @Override
    public void initialize(OptionalValid constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(Optional<?> t, ConstraintValidatorContext constraintValidatorContext) {
        if (t != null && t.isPresent()) {
            return !validator.validate(t.get(), constraintAnnotation.groups()).stream().findAny().isPresent();
        }
        return true;
    }
}
