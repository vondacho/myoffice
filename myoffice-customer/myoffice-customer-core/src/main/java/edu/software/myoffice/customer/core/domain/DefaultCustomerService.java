package edu.software.myoffice.customer.core.domain;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import edu.software.myoffice.customer.core.domain.repository.CustomerRepository;
import edu.software.myoffice.customer.core.domain.repository.FolderRepository;
import edu.software.myoffice.customer.core.domain.model.Customer;
import edu.software.myoffice.customer.core.domain.model.EmailAddress;
import edu.software.myoffice.customer.core.domain.model.Folder;
import edu.software.myoffice.customer.core.domain.model.PhoneNumber;
import edu.software.myoffice.customer.core.domain.validation.ValidationPatterns;
import edu.software.myoffice.customer.core.domain.validation.Validators;
import edu.software.myoffice.customer.core.util.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;

@Slf4j
public class DefaultCustomerService implements CustomerService {

    private final CustomerRepository customerRepository;
    private final FolderRepository folderRepository;

    public DefaultCustomerService(CustomerRepository customerRepository, FolderRepository folderRepository) {
        this.customerRepository = customerRepository;
        this.folderRepository = folderRepository;
    }

    @Override
    public Customer operationA(UUID customerId) {
        return customerRepository.findOneByUuid(customerId)
                .orElseThrow(() -> new EntityNotFoundException(Customer.class, customerId));
    }

    @Override
    public Iterable<Customer> operationB() {
        return customerRepository.findAll();
    }

    @Override
    public Customer save(Customer customer) {
        Validators.validate(customer);

        Folder folder = Folder.builder().name(composeFullName(customer.getFirstName(), customer.getLastName())).build();
        folder.setCustomers(Collections.singletonList(customer));

        return Optional.ofNullable(folderRepository.save(folder).getCustomers()
                .stream()
                .filter(c -> c.equals(customer))
                .findFirst()
                .get())
                .orElseThrow(() -> new EntityNotFoundException(Customer.class, customer.getUuid()));
    }

    @Override
    public Customer save(Customer customer, UUID folderId) {
        Validators.validate(customer);

        Folder folder = folderRepository.findOneByUuid(folderId)
                .orElseThrow(() -> new EntityNotFoundException(Folder.class, folderId));
        folder.getCustomers().add(customer);

        return Optional.ofNullable(folderRepository.save(folder).getCustomers()
                .stream()
                .filter(c -> c.equals(customer))
                .findFirst()
                .get())
                .orElseThrow(() -> new EntityNotFoundException(Customer.class, customer.getUuid()));
    }

    @Override
    public void folderize() {
        customerRepository.findAll().forEach(customer -> {
            log.debug(customer.toString());

            Folder folder = Folder.builder().name(composeFullName(customer.getFirstName(), customer.getLastName())).build();
            folder.setCustomers(Collections.singletonList(customer));
            folderRepository.save(folder);

            log.debug(folder.toString());

            customer.getFolders().add(folder);
            customerRepository.save(customer);
        });
    }

    private String composeFullName(Optional<String> firstName, String lastName) {
        return firstName.isPresent() ? firstName.get() + " " + lastName : lastName;
    }

    @Override
    public void sanitize() {
        customerRepository.findAll().forEach(customer -> {
            log.debug(customer.toString());

            // firstName
            customer.getFirstName().ifPresent(
                    name -> customer.setFirstName(Optional.of(name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase())));
            // lastName
            customer.setLastName(
                    customer.getLastName().substring(0, 1).toUpperCase() + customer.getLastName().substring(1).toLowerCase());
            // phone numbers
            customer.setPhone1(sanitizePhoneNumber(customer.getPhone1()));
            customer.setPhone2(sanitizePhoneNumber(customer.getPhone2()));
            customer.setPhone3(sanitizePhoneNumber(customer.getPhone3()));
            // email addresses
            customer.setEmail1(sanitizeEmailAddress(customer.getEmail1()));
            customer.setEmail2(sanitizeEmailAddress(customer.getEmail2()));
            customer.setEmail3(sanitizeEmailAddress(customer.getEmail3()));

            customerRepository.save(customer);
        });
    }

    private Optional<PhoneNumber> sanitizePhoneNumber(Optional<PhoneNumber> phoneNumber) {
        return phoneNumber.map(p -> {
            Optional<String> sanitized = sanitizePhoneNumber(p.getNumber());
            return sanitized.isPresent() ? p.toBuilder().number(sanitized.get()).build() : null;
        });
    }

    private Optional<String> sanitizePhoneNumber(String phoneNumber) {
        // Extract a phone number
        final Matcher matcher = ValidationPatterns.PHONE_PATTERN.matcher(phoneNumber);
        if (matcher.find()) {
            try {
                // Canonize the original phone number into an international number
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                final String CH_ISO_CODE = "CH";
                final Phonenumber.PhoneNumber parsedNumber = phoneUtil.parse(matcher.group(), CH_ISO_CODE);
                final String canonizedNumber = phoneUtil.format(parsedNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                log.debug("{} has been sanitized into {}", phoneNumber, canonizedNumber);
                return Optional.of(canonizedNumber);
            } catch (NumberParseException e) {
                log.info("{} does not contain a valid phone number that can be parsed", phoneNumber);
            }
        } else {
            log.info("{} does not contain a valid phone number", phoneNumber);
        }
        return Optional.empty();
    }

    private Optional<EmailAddress> sanitizeEmailAddress(Optional<EmailAddress> email) {
        return email.map(e -> {
            Optional<String> sanitized = sanitizeEmailAddress(e.getAddress());
            return sanitized.isPresent() ? e.toBuilder().address(sanitized.get()).build() : null;
        });
    }

    private Optional<String> sanitizeEmailAddress(String emailAddress) {
        // Extract an email address
        final Matcher matcher = ValidationPatterns.EMAIL_PATTERN.matcher(emailAddress);
        if (matcher.find()) {
            log.debug("{} has been sanitized into {}", emailAddress, matcher.group());
            return Optional.of(matcher.group());
        } else {
            log.info("{} does not contain a valid email address", emailAddress);
        }
        return Optional.empty();
    }
}