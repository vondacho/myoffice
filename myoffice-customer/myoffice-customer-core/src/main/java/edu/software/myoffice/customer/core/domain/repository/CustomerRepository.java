package edu.software.myoffice.customer.core.domain.repository;

import edu.software.myoffice.customer.core.domain.model.Customer;

import java.util.Optional;
import java.util.UUID;

public interface CustomerRepository {

    Customer save(Customer customer);

    Optional<Customer> findOneByUuid(UUID uuid);

    Iterable<Customer> findAll();
}
