package edu.software.myoffice.customer.core.domain.model;

import edu.software.myoffice.customer.core.domain.validation.ValidationPatterns;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@Builder(toBuilder = true)
@ToString
public class EmailAddress {

    @Pattern(regexp = ValidationPatterns.EMAIL, message = "'${validatedValue}' does not follow " + ValidationPatterns.EMAIL)
    @NotNull
    private String address;

    private Kind kind;

    public enum Kind {PRIVATE, OFFICE}
}
