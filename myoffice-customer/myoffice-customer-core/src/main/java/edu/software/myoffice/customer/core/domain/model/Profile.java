package edu.software.myoffice.customer.core.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Optional;

@Getter
@Setter
@Builder(toBuilder = true)
@ToString
public class Profile {

    private Optional<Boolean> hasMoved;

    private Optional<Boolean> isSubcontractor;

    private Optional<Boolean> sendInvitation;
}
