package edu.software.myoffice.customer.core.util;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(Class entityClass, String id) {
        super(String.format("No entity of type %s exists with id %s", entityClass.getTypeName(), id));
    }

    public EntityNotFoundException(Class entityClass, UUID id) {
        this(entityClass, id.toString());
    }
}
