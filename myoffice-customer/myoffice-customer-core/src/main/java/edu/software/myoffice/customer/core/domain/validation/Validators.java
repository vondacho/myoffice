/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.software.myoffice.customer.core.domain.validation;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * @author Olivier von Dach.
 */
public class Validators {

    public static final Validator DEFAULT_VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    public static <T> void validate(Validator validator, T object, Class<?> ... groups) {
        throwViolations(validator.validate(object, groups));
    }

    public static <T> void validate(T object, Class<?> ... groups) {
        throwViolations(DEFAULT_VALIDATOR.validate(object, groups));
    }

    public static <T> void throwViolations(Set<ConstraintViolation<T>> constraintViolations) {
        if (!constraintViolations.isEmpty())
            throw new ConstraintViolationException(constraintViolations);
    }
}
