package edu.software.myoffice.customer.core.domain.model;

import edu.software.myoffice.customer.core.util.OptionalValid;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"uuid", "lastName", "firstName"})
@Builder(toBuilder = true)
public class Customer {

    @NotNull
    private UUID uuid;

    private Optional<String> salutation;

    private Optional<String> firstName;

    @NotNull
    private String lastName;

    private Optional<LocalDate> birthDate;

    @NotNull
    private String street;

    @NotNull
    private String zip;

    @NotNull
    private String city;

    private Optional<String> region;

    @NotNull
    private String country;

    @OptionalValid
    private Optional<PhoneNumber> phone1;
    @OptionalValid
    private Optional<PhoneNumber> phone2;
    @OptionalValid
    private Optional<PhoneNumber> phone3;
    @OptionalValid
    private Optional<PhoneNumber> phone4;
    @OptionalValid
    private Optional<EmailAddress> email1;
    @OptionalValid
    private Optional<EmailAddress> email2;
    @OptionalValid
    private Optional<EmailAddress> email3;

    private Optional<String> websiteUrl;

    @NotNull
    @Valid
    private Profile profile;

    @NotNull
    @Valid
    private Social social;

    private Optional<String> notes;

    private List<Folder> folders;

    public static class CustomerBuilder {
        private UUID uuid = UUID.randomUUID();
        private List<Folder> folders = new ArrayList<>();
    }
}
