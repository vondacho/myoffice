package edu.software.myoffice.customer.core.domain.model;

import edu.software.myoffice.customer.core.domain.validation.ValidationPatterns;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Builder(toBuilder = true)
@ToString
public class PhoneNumber {

    @Pattern(regexp = ValidationPatterns.PHONE, message = "'${validatedValue}' does not follow " + ValidationPatterns.PHONE)
    @NotNull
    private String number;

    private Kind kind;

    public enum Kind {PRIVATE, OFFICE, MOBILE, FAX}
}
