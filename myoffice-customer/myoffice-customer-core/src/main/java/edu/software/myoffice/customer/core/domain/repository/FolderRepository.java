package edu.software.myoffice.customer.core.domain.repository;

import edu.software.myoffice.customer.core.domain.model.Folder;

import java.util.Optional;
import java.util.UUID;

public interface FolderRepository {

    Folder save(Folder folder);

    Optional<Folder> findOneByUuid(UUID uuid);

    Iterable<Folder> findAll();
}
