package edu.software.myoffice.customer.core.domain.model;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"uuid", "name"})
@Builder(toBuilder = true)
public class Folder {

    @NotNull
    private UUID uuid;

    @NotNull
    private String name;

    private Optional<String> notes;

    private List<Customer> customers;

    public static class FolderBuilder {
        private UUID uuid = UUID.randomUUID();
        private List<Customer> customers = new ArrayList<>();
    }
}
