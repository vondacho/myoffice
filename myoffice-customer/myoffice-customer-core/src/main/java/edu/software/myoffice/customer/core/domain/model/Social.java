package edu.software.myoffice.customer.core.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Optional;

@Getter
@Setter
@Builder(toBuilder = true)
@ToString
public class Social {

    private Optional<String> skypeUrl;

    private Optional<String> facebookUrl;

    private Optional<String> googleplusUrl;

    private Optional<String> linkedinUrl;

    private Optional<String> twitterUrl;

    private Optional<String> instagramUrl;
}
