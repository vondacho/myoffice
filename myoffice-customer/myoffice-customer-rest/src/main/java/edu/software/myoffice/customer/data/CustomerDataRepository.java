package edu.software.myoffice.customer.data;

import edu.software.myoffice.customer.data.jpa.entity.CustomerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource(path = "/customers", itemResourceRel = "customer", collectionResourceRel = "customers")
public interface CustomerDataRepository {

    @RestResource(path = "byUuid", rel="byUuid")
    Optional<CustomerEntity> findOneByUuid(@Param("uuid") String uuid);

    @RestResource(path = "byLastNameStartingWith", rel="byLastNameStartingWith")
    Page findByLastNameStartingWith(@Param("name") String name, Pageable p);

    @RestResource(path = "byLastNameContaining", rel="byLastNameContaining")
    Page findByLastNameContaining(@Param("name") String name, Pageable p);
}
