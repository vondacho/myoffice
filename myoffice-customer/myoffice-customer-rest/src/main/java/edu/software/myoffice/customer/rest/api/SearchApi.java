package edu.software.myoffice.customer.rest.api;

import io.swagger.annotations.*;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by olivier on 25.12.16.
 */
@Api(value="Search Endpoint", description="Endpoint for the search of customer entities", basePath = "/customers")
public interface SearchApi {

    String SEARCH_CRITERIA_REGEX = "(\\w+?)(:|<|>)(\\w+?),";
    String SORT_DIRECTIVE_REGEX = "(\\w+?):(\\w+?),";

    Pattern SEARCH_CRITERIA_PATTERN = Pattern.compile(SEARCH_CRITERIA_REGEX);
    Pattern SORT_DIRECTIVE_PATTERN = Pattern.compile(SORT_DIRECTIVE_REGEX);

    @ApiOperation(value = "search", nickname = "search")
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "spec",
                    value = "A string of concatenated field criteria, regex: " + SEARCH_CRITERIA_REGEX,
                    required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(
                    name = "sort",
                    value = "A string of concatenated field sorts, regex: " + SORT_DIRECTIVE_REGEX,
                    required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(
                    name = "projection",
                    value = "The name of a predefined projection",
                    required = false, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = List.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Failure")})
    ResponseEntity<?> search(String spec, String sort, String projection);
}