package edu.software.myoffice.customer.projection;

public interface CustomerMixin {

    String getFirstName();

    String getLastName();

    String getStreet();

    String getZip();

    String getCity();

    String getRegion();

    String getCountry();
}
