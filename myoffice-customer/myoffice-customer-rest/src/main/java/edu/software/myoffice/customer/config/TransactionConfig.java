package edu.software.myoffice.customer.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by olivier on 26.12.16.
 */
@Configuration
@EnableTransactionManagement
public class TransactionConfig {
}
