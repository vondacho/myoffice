package edu.software.myoffice.customer.projection;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Value;

import java.util.Optional;

/**
 * http://www.baeldung.com/jackson-json-view-annotation
 */
public interface CustomerProjection {

    @JsonView(Views.MinimalName.class)
    String getUuid();

    @JsonView(Views.MinimalName.class)
    String getLastName();

    @JsonView(Views.ExtendedName.class)
    Optional<String> getFirstName();

    @JsonView(Views.Address.class)
    String getStreet();

    @JsonView(Views.Address.class)
    String getZip();

    @JsonView(Views.Address.class)
    String getCity();

    @JsonView(Views.Address.class)
    String getRegion();

    @JsonView(Views.Address.class)
    String getCountry();

    @JsonView(Views.Folder.class)
    @Value("#{target.folders}")
    Iterable<FolderNameProjection> getFolders();
}
