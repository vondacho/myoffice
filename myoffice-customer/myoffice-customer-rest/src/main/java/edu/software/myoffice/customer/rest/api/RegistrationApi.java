package edu.software.myoffice.customer.rest.api;

import edu.software.myoffice.customer.core.domain.model.Customer;
import io.swagger.annotations.*;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;

/**
 * Created by olivier on 29.12.16.
 */
@Api(value="Registration Endpoint", description="Endpoint for the creation of customer entities", basePath = "/customers")
public interface RegistrationApi {

    @ApiOperation(value = "create", nickname = "create")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Customer.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Failure")})
    ResponseEntity<?> create(Customer customer);
}
