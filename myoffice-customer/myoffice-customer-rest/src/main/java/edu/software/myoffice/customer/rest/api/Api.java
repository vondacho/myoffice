package edu.software.myoffice.customer.rest.api;

/**
 * Created by olivier on 25.12.16.
 */
public interface Api extends SearchApi, MaintenanceApi, RegistrationApi {
    String BASE_URL = "/api/v1";
    String CUSTOMER_ENDPOINT = "/customers";
    String FOLDER_ENDPOINT = "/folders";
}
