package edu.software.myoffice.customer.projection;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Value;

import java.util.Optional;

/**
 * http://www.baeldung.com/jackson-json-view-annotation
 */
public interface CustomerNameProjection {

    @JsonView(Views.MinimalName.class)
    String getLastName();

    @JsonView(Views.ExtendedName.class)
    Optional<String> getFirstName();
}
