package edu.software.myoffice.customer.data;

import edu.software.myoffice.customer.data.jpa.entity.FolderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource(path = "/folders", itemResourceRel = "folders", collectionResourceRel = "folders")
public interface FolderDataRepository {

    @RestResource
    Optional<FolderEntity> findOneByUuid(@Param("uuid") String uuid);

    @RestResource
    Page findPagedByNameStartingWith(@Param("name") String name, Pageable p);

    @RestResource
    Page findPagedByNameContaining(@Param("name") String name, Pageable p);
}
