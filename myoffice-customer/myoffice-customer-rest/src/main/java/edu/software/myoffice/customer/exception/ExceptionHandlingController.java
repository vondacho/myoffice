/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.software.myoffice.customer.exception;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Olivier von Dach on 29.12.16.
 */
@ControllerAdvice
public class ExceptionHandlingController {

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public List<ErrorModel> handleConstraintViolation(
            HttpServletRequest req, final ConstraintViolationException exception) {

        return exception.getConstraintViolations().stream().map(violation ->
                new ErrorModel(
                        violation.getPropertyPath().toString(),
                        violation.getMessage(),
                        violation.getInvalidValue())).collect(Collectors.toList());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    public ErrorModel handleNotReadableMessage(
            HttpServletRequest req, final HttpMessageNotReadableException exception) {

        return new ErrorModel(exception);
    }

    @AllArgsConstructor
    private static class ErrorModel {
        public String field;
        public String message;
        public Object invalidValue;

        ErrorModel(Exception e) {
            this("", e.getMessage(), null);
        }
    }
}
