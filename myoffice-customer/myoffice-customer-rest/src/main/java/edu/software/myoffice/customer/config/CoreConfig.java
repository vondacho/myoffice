package edu.software.myoffice.customer.config;

import edu.software.myoffice.customer.core.domain.CustomerService;
import edu.software.myoffice.customer.core.domain.DefaultCustomerService;
import edu.software.myoffice.customer.core.domain.repository.CustomerRepository;
import edu.software.myoffice.customer.core.domain.repository.FolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by olivier.
 */
@Configuration
public class CoreConfig {

    private final CustomerRepository customerRepository;

    private final FolderRepository folderRepository;

    @Autowired
    public CoreConfig(CustomerRepository customerRepository, FolderRepository folderRepository) {
        this.customerRepository = customerRepository;
        this.folderRepository = folderRepository;
    }

    @Bean
    public CustomerService customerService() {
        return new DefaultCustomerService(customerRepository, folderRepository);
    }
}
