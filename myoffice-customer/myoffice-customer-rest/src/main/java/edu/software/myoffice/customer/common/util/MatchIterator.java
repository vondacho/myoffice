package edu.software.myoffice.customer.common.util;

import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;

/**
 * Created by olivier on 28.12.16.
 */
public class MatchIterator extends Spliterators.AbstractSpliterator<MatchResult> {
    private final Matcher matcher;

    public MatchIterator(Matcher m) {
        super(Long.MAX_VALUE, ORDERED | NONNULL);
        matcher = m;
    }

    public boolean tryAdvance(Consumer<? super MatchResult> action) {
        if (!matcher.find()) return false;
        action.accept(matcher.toMatchResult());
        return true;
    }
}