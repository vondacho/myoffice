package edu.software.myoffice.customer.projection;

import edu.software.myoffice.customer.data.jpa.entity.CustomerEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by olivier on 23.12.16.
 */
@Projection(name = "address", types = { CustomerEntity.class })
public interface CustomerAddressProjection {

    String getFirstName();

    String getLastName();

    @Value("#{target.firstName} #{target.lastName}")
    String getFullName();

    String getStreet();

    String getZip();

    String getCity();

    String getRegion();

    String getCountry();
}
