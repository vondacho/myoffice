package edu.software.myoffice.customer.rest;

import com.fasterxml.jackson.annotation.JsonView;
import edu.software.myoffice.customer.core.domain.CustomerService;
import edu.software.myoffice.customer.core.domain.model.Customer;
import edu.software.myoffice.customer.core.util.EntityNotFoundException;
import edu.software.myoffice.customer.data.jpa.repository.CustomerJpaRepository;
import edu.software.myoffice.customer.projection.CustomerNameProjection;
import edu.software.myoffice.customer.projection.CustomerProjection;
import edu.software.myoffice.customer.projection.Views;
import edu.software.myoffice.customer.rest.api.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = Api.CUSTOMER_ENDPOINT)
public class TestEndpoint {

    @Autowired
    private CustomerJpaRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/{id}/test1", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> test1(@PathParam(value = "id") String uuid) {
        return ok(customerRepository.findOneByUuid(uuid)
                .orElseThrow(() -> new EntityNotFoundException(Customer.class, uuid)));
    }

    @RequestMapping(value = "/{id}/test2", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> test2(@PathParam(value = "id") String uuid) {
        Optional<CustomerProjection> customer = customerRepository.findOneProjectedByUuid(uuid, CustomerProjection.class);
        return ok(customer.orElseThrow(() -> new EntityNotFoundException(Customer.class, uuid)));
    }

    @RequestMapping(value = "/{id}/test3", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> test3(@PathParam(value = "id") String uuid) {
        return ok(customerService.operationA(UUID.fromString(uuid)));
    }

    @JsonView(Views.MinimalName.class)
    @RequestMapping(value = "/test2", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> test2All() {
        return ok(customerRepository.findAllProjectedBy(CustomerNameProjection.class));
    }

    @JsonView(Views.ExtendedName.class)
    @RequestMapping(value = "/test3", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> test3All() {
        return ok(customerRepository.findAllProjectedBy(CustomerNameProjection.class));
    }

    @JsonView(Views.Address.class)
    @RequestMapping(value = "/test4", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> test6All() {
        return ok(customerRepository.findAllProjectedBy(CustomerProjection.class));
    }

    @JsonView(Views.Folder.class)
    @RequestMapping(value = "/{id}/folder", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> test5(@PathParam(value = "id") String uuid) {
        Optional<CustomerProjection> customer = customerRepository.findOneProjectedByUuid(uuid, CustomerProjection.class);
        return ok(customer.orElseThrow(() -> new EntityNotFoundException(Customer.class, uuid)));
    }

    @JsonView(Views.MinimalName.class)
    @RequestMapping(value = "/test6", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> test8All() {
        return ok(customerService.operationB());
    }
}

