package edu.software.myoffice.customer.projection;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Created by olivier on 23.12.16.
 */
public interface FolderNameProjection {

    @JsonView(Views.MinimalName.class)
    String getName();
}
