package edu.software.myoffice.customer.rest;

import edu.software.myoffice.customer.core.domain.CustomerService;
import edu.software.myoffice.customer.rest.api.MaintenanceApi;
import edu.software.myoffice.customer.rest.api.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

/**
 * Created by olivier on 25.12.16.
 */
@Slf4j
@RestController
@RequestMapping(value = Api.BASE_URL)
public class MaintenanceEndpoint implements MaintenanceApi {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = Api.CUSTOMER_ENDPOINT + "/folderize", method = RequestMethod.POST)
    @ResponseStatus
    public ResponseEntity<?> folderize() {
        customerService.folderize();
        return ok().build();
    }

    @RequestMapping(value = Api.CUSTOMER_ENDPOINT + "/sanitize", method = RequestMethod.POST)
    @ResponseStatus
    public ResponseEntity<?> sanitize() {
        customerService.sanitize();
        return ok().build();
    }
}

