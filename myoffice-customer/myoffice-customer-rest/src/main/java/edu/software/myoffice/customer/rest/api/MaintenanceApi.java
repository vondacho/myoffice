package edu.software.myoffice.customer.rest.api;

import io.swagger.annotations.*;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;

/**
 * Created by olivier on 29.12.16.
 */
@Api(value="Maintenance Endpoint", description="Endpoint for the maintenance of customer and folder entities", basePath = "/customers")
public interface MaintenanceApi {

    @ApiOperation(value = "folderize", nickname = "folderize", notes = "Link every orphan customer to a new created folder", consumes="*/*")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Failure")})
    ResponseEntity<?> folderize();

    @ApiOperation(value = "sanitize", nickname = "sanitize", notes = "Clean all email addresses and phone numbers", consumes="*/*")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Failure")})
    ResponseEntity<?> sanitize();
}
