/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.software.myoffice.customer.data;

import edu.software.myoffice.customer.data.jpa.entity.CustomerEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

/**
 * @author Olivier von Dach on 29.12.16.
 */
@Slf4j
@Component
@RepositoryEventHandler(CustomerEntity.class)
public class CustomerEventHandler {

    @HandleBeforeCreate
    public void handleBeforeCreate(CustomerEntity p) {
        log.debug("before customer creation {}", p);
    }

    @HandleBeforeSave
    public void handleBeforeSave(CustomerEntity p) {
        log.debug("before customer mutation {}", p);
    }
}
