package edu.software.myoffice.customer.rest;

import edu.software.myoffice.customer.common.util.MatchIterator;
import edu.software.myoffice.customer.data.jpa.repository.CustomerJpaRepository;
import edu.software.myoffice.customer.projection.CustomerAddressProjection;
import edu.software.myoffice.customer.data.CustomerDataRepository;
import edu.software.myoffice.customer.data.jpa.query.SortBuilder;
import edu.software.myoffice.customer.data.jpa.query.SpecificationsBuilder;
import edu.software.myoffice.customer.rest.api.Api;
import edu.software.myoffice.customer.rest.api.SearchApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.stream.StreamSupport;

import static org.springframework.hateoas.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RepositoryRestController
@RequestMapping(value = Api.CUSTOMER_ENDPOINT)
public class SearchEndpoint implements SearchApi {

    @Autowired
    private CustomerJpaRepository customerRepository;

    @RequestMapping(value = "/search/criteria", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> search(@RequestParam(value = "spec") String spec,
                                    @RequestParam(value = "sort", required = false) String sort,
                                    @RequestParam(value = "projection", required = false) String projection) {

        SpecificationsBuilder specBuilder = new SpecificationsBuilder();
        StreamSupport.stream(new MatchIterator(SearchApi.SEARCH_CRITERIA_PATTERN.matcher(spec + ",")), false).
                forEach(match -> specBuilder.with(match.group(1), match.group(2), match.group(3)));

        SortBuilder sortBuilder = new SortBuilder();
        StreamSupport.stream(new MatchIterator(SearchApi.SORT_DIRECTIVE_PATTERN.matcher(sort + ",")), false).
                forEach(match -> sortBuilder.with(match.group(1), match.group(2)));

        Resources<CustomerAddressProjection> resources = new Resources<>(customerRepository.findAll(specBuilder.build(), sortBuilder.build()));
        resources.add(linkTo(methodOn(SearchEndpoint.class).search(spec, sort, projection)).withSelfRel());

        return ok(resources);
    }
}

