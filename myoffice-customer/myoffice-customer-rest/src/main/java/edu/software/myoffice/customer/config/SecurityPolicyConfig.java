package edu.software.myoffice.customer.config;

import edu.software.myoffice.customer.rest.api.Api;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by olivier on 25.12.16.
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityPolicyConfig extends WebSecurityConfigurerAdapter {

    /**
     * This section defines the user accounts which can be used for
     * authentication as well as the roles each user has.
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("o").password("o").roles("USER").and()
                .withUser("a").password("a").roles("USER", "ADMIN");
    }

    /**
     * This section defines the security policy for the app.
     * - BASIC authentication is supported (enough for this REST-based demo)
     * - /employees is secured using URL security shown below
     * - CSRF headers are disabled since we are only testing the REST interface,
     * not a web one.
     * <p>
     * NOTE: GET is not shown which defaults to permitted.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().and()
                .authorizeRequests()
                .regexMatchers(HttpMethod.GET, "\\A/customers.*\\Z").hasRole("USER")
                .antMatchers(HttpMethod.POST, Api.BASE_URL).hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, Api.BASE_URL + "/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, Api.BASE_URL + "/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, Api.BASE_URL + "/**").hasRole("ADMIN").and()
                .csrf().disable();
    }
}
