package edu.software.myoffice.customer.rest;

import edu.software.myoffice.customer.core.domain.CustomerService;
import edu.software.myoffice.customer.core.domain.model.Customer;
import edu.software.myoffice.customer.data.jpa.entity.CustomerEntity;
import edu.software.myoffice.customer.rest.api.RegistrationApi;
import edu.software.myoffice.customer.rest.api.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by olivier on 25.12.16.
 */
@Slf4j
@RepositoryRestController
@RequestMapping(value = Api.CUSTOMER_ENDPOINT)
public class RegistrationEndpoint implements RegistrationApi {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private RepositoryEntityLinks entityLinks;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody Customer customer) {
        final Customer response = customerService.save(customer);

        Resource<Customer> resource = new Resource<>(response);
        resource.add(entityLinks.linkForSingleResource(CustomerEntity.class, response.getUuid()).withSelfRel());
        resource.add(entityLinks.linkToSingleResource(CustomerEntity.class, response.getUuid()).withRel("customer"));
        resource.add(entityLinks.linkForSingleResource(CustomerEntity.class, response.getUuid()).slash("folders").withRel("folders"));
        return new ResponseEntity(resource, HttpStatus.CREATED);
    }
}

