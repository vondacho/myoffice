/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.software.myoffice.customer.config;

import edu.software.myoffice.customer.core.domain.model.Customer;
import edu.software.myoffice.customer.projection.CustomerMixin;
import edu.software.myoffice.customer.projection.CustomerProjection;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * @author Olivier von Dach
 */
@Configuration
public class ObjectMapperConfig {
    /*
    @Bean
    public SimpleModule customModule() {
        return new SimpleModule() {
            @Override
            public void setupModule(SetupContext context) {
                context.addAbstractTypeResolver(
                        new SimpleAbstractTypeResolver().addMapping(Customer.class, CustomerEntity.class));
            }
        };
    }
    */

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        return new Jackson2ObjectMapperBuilder().mixIn(Customer.class, CustomerProjection.class);
    }
}
