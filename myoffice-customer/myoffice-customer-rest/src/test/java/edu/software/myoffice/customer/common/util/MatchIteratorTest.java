package edu.software.myoffice.customer.common.util;

import edu.software.myoffice.customer.core.domain.validation.ValidationPatterns;
import org.junit.Test;

import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.collection.IsIn.isIn;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

/**
 * Created by olivier on 28.12.16.
 */
public class MatchIteratorTest {
    @Test
    public void chainOfEmailsTest() {
        final String text = "aa.bb@cc.dd,ee@ff.gg";

        List<String> matches = StreamSupport.stream(new MatchIterator(ValidationPatterns.EMAIL_PATTERN.matcher(text)), false).
                map(MatchResult::group).
                collect(Collectors.toList());

        assertThat("aa.bb@cc.dd", isIn(matches));
        assertThat("ee@ff.gg", isIn(matches));
        assertThat("e@ff.gg", not(isIn(matches)));
    }

    @Test
    public void chainOfPhonesTest() {
        final String text = "+419978887766 0997776655";
        List<String> matches = StreamSupport.stream(new MatchIterator(ValidationPatterns.PHONE_PATTERN.matcher(text)), false).
                map(MatchResult::group).
                collect(Collectors.toList());

        assertThat("+419978887766", isIn(matches));
        assertThat("0997776655", isIn(matches));
        assertThat("099777665", not(isIn(matches)));
    }

    @Test
    public void chainOfCriteriaTest() {
        final String text = "lastName:Doe,firstName:John,age<20,";
        final Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");

        List<MatchResult> matches = StreamSupport.stream(new MatchIterator(pattern.matcher(text)), false).
                collect(Collectors.toList());

        assertThat(matches, is(iterableWithSize(3)));
        assertThat(matches.get(0).groupCount(), is(3));
        assertThat(matches.get(0).group(1), is("lastName"));
        assertThat(matches.get(0).group(2), is(":"));
        assertThat(matches.get(0).group(3), is("Doe"));
        assertThat(matches.get(1).groupCount(), is(3));
        assertThat(matches.get(1).group(1), is("firstName"));
        assertThat(matches.get(1).group(2), is(":"));
        assertThat(matches.get(1).group(3), is("John"));
        assertThat(matches.get(2).groupCount(), is(3));
        assertThat(matches.get(2).group(1), is("age"));
        assertThat(matches.get(2).group(2), is("<"));
        assertThat(matches.get(2).group(3), is("20"));
    }

    @Test
    public void chainOfSortsTest() {
        final String text = "firstName:desc,lastName:asc,";
        final Pattern pattern = Pattern.compile("(\\w+?):(\\w+?),");

        List<MatchResult> matches = StreamSupport.stream(new MatchIterator(pattern.matcher(text)), false).
                collect(Collectors.toList());

        assertThat(matches, is(iterableWithSize(2)));
        assertThat(matches.get(0).groupCount(), is(2));
        assertThat(matches.get(0).group(1), is("firstName"));
        assertThat(matches.get(0).group(2), is("desc"));
        assertThat(matches.get(1).groupCount(), is(2));
        assertThat(matches.get(1).group(1), is("lastName"));
        assertThat(matches.get(1).group(2), is("asc"));
    }
}
