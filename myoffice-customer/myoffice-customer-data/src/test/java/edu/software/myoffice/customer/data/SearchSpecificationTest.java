package edu.software.myoffice.customer.data;

import edu.software.myoffice.customer.core.domain.model.EmailAddress;
import edu.software.myoffice.customer.core.domain.model.PhoneNumber;
import edu.software.myoffice.customer.data.jpa.entity.CustomerEntity;
import edu.software.myoffice.customer.data.jpa.query.SearchSpecificationFactory;
import edu.software.myoffice.customer.data.jpa.repository.CustomerJpaRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.hamcrest.collection.IsIn.isIn;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

/**
 * Created by olivier on 27.12.16.
 * http://www.baeldung.com/rest-api-search-language-spring-data-specifications
 */
@RunWith(SpringRunner.class)
@DataJpaTest(showSql = true)
@Transactional
public class SearchSpecificationTest {

    @Autowired
    private CustomerJpaRepository customerRepository;

    private CustomerEntity john;
    private CustomerEntity tom;

    @Before
    public void init() {
        customerRepository.save(john = CustomerEntity.builder()
                .firstName("John")
                .lastName("Doe")
                .emailAddress1("john@doe.com")
                .emailKind1(EmailAddress.Kind.OFFICE)
                .phoneNumber1("+419912223344")
                .phoneKind1(PhoneNumber.Kind.OFFICE)
                .city("NY")
                .zip("10000")
                .country("USA").build());

        customerRepository.save(tom = CustomerEntity.builder()
                .firstName("Tom")
                .lastName("Doe")
                .emailAddress1("tom@doe.com")
                .emailKind1(EmailAddress.Kind.OFFICE)
                .phoneNumber1("+419987776655")
                .phoneKind1(PhoneNumber.Kind.OFFICE)
                .city("NY")
                .zip("10000")
                .country("USA").build());
    }

    @Test
    public void givenLast_whenGettingListOfUsers_thenCorrect() {
        Specification<CustomerEntity> spec = SearchSpecificationFactory.create("lastName", ":", "doe");
        List<CustomerEntity> results = customerRepository.findAll(spec);
        assertThat(john, isIn(results));
        assertThat(tom, isIn(results));
    }

    @Test
    public void givenFirstAndLastName_whenGettingListOfUsers_thenCorrect() {
        Specification<CustomerEntity> spec1 = SearchSpecificationFactory.create("firstName", ":", "john");
        Specification<CustomerEntity> spec2 = SearchSpecificationFactory.create("lastName", ":", "doe");
        List<CustomerEntity> results = customerRepository.findAll(Specifications.where(spec1).and(spec2));
        assertThat(john, isIn(results));
        assertThat(tom, not(isIn(results)));
    }

    @Test
    public void givenWrongFirstAndLast_whenGettingListOfUsers_thenCorrect() {
        Specification<CustomerEntity> spec1 = SearchSpecificationFactory.create("firstName", ":", "Adam");
        Specification<CustomerEntity> spec2 = SearchSpecificationFactory.create("lastName", ":", "Fox");
        List<CustomerEntity> results = customerRepository.findAll(Specifications.where(spec1).and(spec2));
        assertThat(john, not(isIn(results)));
        assertThat(tom, not(isIn(results)));
    }

    @Test
    public void givenPartialFirst_whenGettingListOfUsers_thenCorrect() {
        Specification<CustomerEntity> spec = SearchSpecificationFactory.create("firstName", ":", "jo");
        List<CustomerEntity> results = customerRepository.findAll(spec);
        assertThat(john, isIn(results));
        assertThat(tom, not(isIn(results)));
    }
}
