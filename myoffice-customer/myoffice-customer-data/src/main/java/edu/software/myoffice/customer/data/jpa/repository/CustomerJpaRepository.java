package edu.software.myoffice.customer.data.jpa.repository;

import edu.software.myoffice.customer.data.jpa.entity.CustomerEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface CustomerJpaRepository extends PagingAndSortingRepository<CustomerEntity, Long>, JpaSpecificationExecutor<CustomerEntity> {

    Optional<CustomerEntity> findOneByUuid(String uuid);

    <P> Optional<P> findOneProjectedByUuid(String uuid, Class<P> projection);

    <P> List<P> findProjectedByUuid(Iterable<String> uuid, Class<P> projection);

    <P> List<P> findProjectedByLastNameStartingWith(String name, Class<P> projection);

    <P> List<P> findProjectedByLastNameContaining(String name, Class<P> projection);

    <P> List<P> findAllProjectedBy(Class<P> projection);

    List<CustomerEntity> findAll();

    List<CustomerEntity> findAll(Specification specification);
}