package edu.software.myoffice.customer.data.jpa.entity;

import edu.software.myoffice.customer.core.domain.model.EmailAddress;
import edu.software.myoffice.customer.core.domain.model.PhoneNumber;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Entity(name = "Customer")
@Getter
@Setter
@EqualsAndHashCode(of = {"uuid", "firstName", "lastName"})
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerEntity extends AuditableEntity<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String uuid;
    private String salutation;
    private String firstName;
    @NotNull
    private String lastName;
    private LocalDate birthDate;
    private String street;
    @NotNull
    private String zip;
    @NotNull
    private String city;
    private String region;
    @NotNull
    private String country;

    private String phoneNumber1;
    @Enumerated(EnumType.STRING)
    private PhoneNumber.Kind phoneKind1;

    private String phoneNumber2;
    @Enumerated(EnumType.STRING)
    private PhoneNumber.Kind phoneKind2;

    private String phoneNumber3;
    @Enumerated(EnumType.STRING)
    private PhoneNumber.Kind phoneKind3;

    private String phoneNumber4;
    @Enumerated(EnumType.STRING)
    private PhoneNumber.Kind phoneKind4;

    private String emailAddress1;
    @Enumerated(EnumType.STRING)
    private EmailAddress.Kind emailKind1;

    private String emailAddress2;
    @Enumerated(EnumType.STRING)
    private EmailAddress.Kind emailKind2;

    private String emailAddress3;
    @Enumerated(EnumType.STRING)
    private EmailAddress.Kind emailKind3;

    private String websiteUrl;

    private String skypeUrl;
    private String facebookUrl;
    private String googleplusUrl;
    private String linkedinUrl;
    private String twitterUrl;
    private String instagramUrl;

    private Boolean hasMoved;
    private Boolean isSubcontractor;
    private Boolean sendInvitation;

    private String notes;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "Customer_Folder",
            joinColumns = {
                    @JoinColumn(
                            name = "customer_id",
                            referencedColumnName = "id"
                    )
            },
            inverseJoinColumns = {
                    @JoinColumn(
                            name = "folder_id",
                            referencedColumnName = "id"
                    )
            }
    )
    private List<FolderEntity> folders = new ArrayList<>();

    public String getUuid() {
        return uuid != null ? uuid : (uuid = UUID.randomUUID().toString());
    }

    public Optional<String> getSalutation() {
        return Optional.ofNullable(salutation);
    }

    public Optional<String> getFirstName() {
        return Optional.ofNullable(firstName);
    }

    public Optional<LocalDate> getBirthDate() {
        return Optional.ofNullable(birthDate);
    }

    public Optional<String> getRegion() {
        return Optional.ofNullable(region);
    }

    public Optional<String> getPhoneNumber1() {
        return Optional.ofNullable(phoneNumber1);
    }

    public Optional<String> getPhoneNumber2() {
        return Optional.ofNullable(phoneNumber2);
    }

    public Optional<String> getPhoneNumber3() {
        return Optional.ofNullable(phoneNumber3);
    }

    public Optional<String> getPhoneNumber4() {
        return Optional.ofNullable(phoneNumber4);
    }

    public Optional<String> getEmailAddress1() {
        return Optional.ofNullable(emailAddress1);
    }

    public Optional<String> getEmailAddress2() {
        return Optional.ofNullable(emailAddress2);
    }

    public Optional<String> getEmailAddress3() {
        return Optional.ofNullable(emailAddress3);
    }

    public Optional<String> getWebsiteUrl() {
        return Optional.ofNullable(websiteUrl);
    }

    public Optional<String> getSkypeUrl() {
        return Optional.ofNullable(skypeUrl);
    }

    public Optional<String> getFacebookUrl() {
        return Optional.ofNullable(facebookUrl);
    }

    public Optional<String> getGoogleplusUrl() {
        return Optional.ofNullable(googleplusUrl);
    }

    public Optional<String> getLinkedinUrl() {
        return Optional.ofNullable(linkedinUrl);
    }

    public Optional<String> getTwitterUrl() {
        return Optional.ofNullable(twitterUrl);
    }

    public Optional<String> getInstagramUrl() {
        return Optional.ofNullable(instagramUrl);
    }

    public Optional<Boolean> getHasMoved() {
        return Optional.ofNullable(hasMoved);
    }

    public Optional<Boolean> getIsSubcontractor() {
        return Optional.ofNullable(isSubcontractor);
    }

    public Optional<Boolean> getSendInvitation() {
        return Optional.ofNullable(sendInvitation);
    }

    public Optional<String> getNotes() {
        return Optional.ofNullable(notes);
    }
}
