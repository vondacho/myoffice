package edu.software.myoffice.customer.data.jpa.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Entity(name = "Folder")
@Getter
@Setter
@EqualsAndHashCode(of = {"name", "uuid"})
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FolderEntity extends AuditableEntity<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String uuid;
    @NotNull
    private String name;
    private String notes;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<CustomerEntity> customers;

    public String getUuid() {
        return uuid != null ? uuid : (uuid = UUID.randomUUID().toString());
    }

    public Optional<String> getNotes() {
        return Optional.ofNullable(notes);
    }
}
