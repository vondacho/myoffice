package edu.software.myoffice.customer.data.jpa.query;

import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by olivier on 27.12.16.
 */
public class SortBuilder {
    private List<Sort.Order> params;

    public SortBuilder() {
        params = new ArrayList<Sort.Order>();
    }

    public SortBuilder with(String property, String direction) {
        params.add(new Sort.Order(Sort.Direction.fromString(direction), property));
        return this;
    }

    public Sort build() {
        if (params.size() == 0) {
            return null;
        }
        return new Sort(params);
    }
}
