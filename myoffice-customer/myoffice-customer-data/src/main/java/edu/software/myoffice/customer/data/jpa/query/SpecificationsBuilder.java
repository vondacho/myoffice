package edu.software.myoffice.customer.data.jpa.query;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by olivier on 27.12.16.
 * http://www.baeldung.com/rest-api-search-language-spring-data-specifications
 */
public class SpecificationsBuilder<E> {
    private final List<Specification<E>> specs;

    public SpecificationsBuilder() {
        specs = new ArrayList<>();
    }

    public SpecificationsBuilder with(String key, String operation, Object value) {
        specs.add(SearchSpecificationFactory.create(key, operation, value));
        return this;
    }

    public Specification<E> build() {
        if (specs.size() == 0) {
            return null;
        }

        Specification<E> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }
}
