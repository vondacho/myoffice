package edu.software.myoffice.customer.data.jpa.repository;

import edu.software.myoffice.customer.data.jpa.entity.FolderEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface FolderJpaRepository extends PagingAndSortingRepository<FolderEntity, Long> {

    <P> Optional<P> findOneProjectedByUuid(String uuid, Class<P> projection);

    <P> List<P> findProjectedByUuid(Iterable<String> uuid, Class<P> projection);

    <P> List<P> findProjectedByNameStartingWith(String name, Class<P> projection);

    <P> List<P> findProjectedByNameContaining(String name, Class<P> projection);

    <P> List<P> findAllProjectedBy(Class<P> projection);
}
