package edu.software.myoffice.customer.data.jpa.query;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Created by olivier on 27.12.16.
 * http://www.baeldung.com/rest-api-search-language-spring-data-specifications
 */
public class SearchSpecificationFactory {

    public static <E> Specification<E> create(final String key, final String operation, final Object value) {

        return (Root<E> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            if (operation.equalsIgnoreCase(":")) {

                if (String.class == root.get(key).getJavaType()) {
                    return builder.like(root.<String>get(key), "%" + value + "%");
                } else {
                    return builder.equal(root.get(key), value);
                }

            } else if (operation.equalsIgnoreCase(">")) {
                return builder.greaterThanOrEqualTo(root.<String>get(key), value.toString());

            } else if (operation.equalsIgnoreCase("<")) {
                return builder.lessThanOrEqualTo(root.<String>get(key), value.toString());

            }
            return null;
        };
    }
}
