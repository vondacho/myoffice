/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.software.myoffice.customer.data.adapter;

import edu.software.myoffice.customer.core.domain.repository.CustomerRepository;
import edu.software.myoffice.customer.core.domain.model.Customer;
import edu.software.myoffice.customer.data.jpa.repository.CustomerJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Olivier von Dach.
 */
@Component
public class CustomerRepositoryAdapter implements CustomerRepository {

    private final CustomerJpaRepository repository;

    @Autowired
    public CustomerRepositoryAdapter(CustomerJpaRepository repository) {
        this.repository = repository;
    }

    @Override
    public Customer save(Customer customer) {
        return null;
    }

    @Override
    public Optional<Customer> findOneByUuid(UUID uuid) {
        return null;
    }

    @Override
    public Iterable<Customer> findAll() {
        return repository.findAll().stream().map(
            e -> Customer.builder()
                    .firstName(e.getFirstName())
                    .lastName(e.getLastName())
                    .zip(e.getZip())
                    .city(e.getCity())
                    .country(e.getCountry())
                    .build())
                .collect(Collectors.toList());
    }
}
