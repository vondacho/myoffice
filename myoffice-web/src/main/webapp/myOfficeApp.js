angular.module('myOfficeApp',  ['ngTable','angular-click-outside'])

   
    .controller('myOfficeController', ['$scope','$http','$log',  'ngTableParams', function ($scope,$http,$log,ngTableParams) {
		
		$scope.isEditing=false;
		$scope.isEditingExisting=false;
		$scope.name="";
		$scope.lastName="";
		$scope.birthDate;
		$scope.customer={};
		$scope.customerResult=null;
		$scope.editedCustomer=null;
		$scope.editedIndex=null;
		$scope.selectedIndex=null;
		$scope.event=null;
		$scope.tableParams = null;
		
		function init(){ 
			var customer='http://localhost:9090/api/customers';
			$scope.tableParams = new ngTableParams({
						page: 1,            // show first page
						count: 5           // count per page
						} ,{
						counts: [] ,
						getData: function ($defer, params) {
									var page = params.page();
									var size = params.count();
									$http.get(customer).
											success(function(data) {												
												$defer.resolve(data.content.slice((params.page() - 1) * params.count(), params.page() * params.count()));
												
											});								
								}
						
						});					
				
		}
		
		function startEditing(){
			$scope.isEditing=true;			
		}
		function startEditingExisting(customerIndex){
			$scope.isEditingExisting=true;
			$scope.selectedIndex=null;
			setEditedCustomer(customerIndex);
			console.log('startEditingExisting editedCustomer '+angular.fromJson($scope.editedCustomer));
		}		

		function stopEditing(){
			 console.log('stopEditing ');
			 $scope.isEditing=false;
			 deselectCustomer();
		}
		function stopEditingExisting(){
			console.log('stopEditingExisting ');
			$scope.isEditingExisting=false;
			deselectCustomer();
		}
		function shouldShowEditing(){
			return $scope.isEditing;
		}
		function shouldShowEditingExisting(){
			return $scope.isEditingExisting;
		}
		
		function ValidateEmail(mail) 
		{
			 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
			  {
				return (true);
			  }
		}
		function deleteExisting(customerIndex){		
			var customer='http://localhost:9090/api/customers';
			console.log("url to delete "+customer+'/'+$scope.tableParams.data[customerIndex].id);
			console.log("Taille avant "+$scope.tableParams.data.length);
			$http.delete(customer+'/'+$scope.tableParams.data[customerIndex].id);
			$http.get(customer).success(function(data) {	
									$scope.tableParams.total(data.content.length);
									$scope.tableParams.reload();
											});
			
			$scope.tableParams.reload();
			console.log("Taille Apres "+$scope.tableParams.data.length);			
			$scope.editedCustomer=null;
			$scope.editedIndex=null;
			$scope.selectedIndex=null;
		}
		function addCustomer(customer){
			$scope.customerResult.push(customer);
			stopEditing();
			toastr.success('customer created successfully!');
			console.log(customer);
			console.log($scope.customerResult);
		}
		function setEditedCustomer(customerIndex){
			$scope.editedIndex=customerIndex;
			$scope.selectedIndex=customerIndex;
			console.log('ngTableParam item '+customerIndex+' '+$scope.tableParams.data[customerIndex]);	
			$scope.editedCustomer= angular.copy($scope.tableParams.data[customerIndex]);			
		}
		function deselectCustomer(){
		$scope.editedIndex=null;
		$scope.selectedIndex=null;
		$scope.editedCustomer=null;
		}
		function updateCustomer(){
		var customer='http://localhost:9090/api/customers';
		console.log("url to update "+customer+'/'+$scope.tableParams.data[$scope.editedIndex].id);
		console.log("data to send "+JSON.stringify($scope.editedCustomer));
		$http.put(customer+'/'+$scope.tableParams.data[$scope.editedIndex].id,$scope.editedCustomer);
		
		
		$scope.editedIndex=null;
		$scope.selectedIndex=null;
		stopEditingExisting();
		console.log('customer updated successfully!');
		toastr.success('customer updated successfully!');
		}
				 
		$scope.init=init; 
		$scope.startEditing=startEditing;
		$scope.stopEditing=stopEditing;
		$scope.shouldShowEditing=shouldShowEditing;
		$scope.startEditingExisting=startEditingExisting;
		$scope.stopEditingExisting=stopEditingExisting;
		$scope.shouldShowEditingExisting=shouldShowEditingExisting;
		$scope.addCustomer=addCustomer;
		$scope.ValidateEmail=ValidateEmail;
		$scope.setEditedCustomer=setEditedCustomer;
		$scope.updateCustomer=updateCustomer;
		$scope.deleteExisting=deleteExisting;
		$scope.deselectCustomer=deselectCustomer;
		
		$scope.sortType     = 'name'; // set the default sort type
		$scope.sortReverse  = false;  // set the default sort order
		$scope.$log = $log;
  
		
    }])